import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class play here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class play extends World
{

    /**
     * Constructor for objects of class play.
     * 
     */
  
    public play()
    {    
        super(400, 600, 1); 
        prepare();
    }

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {              
        for(int i=0; i<25;i++){            
            addObject(new bataplay(),i*20,Greenfoot.getRandomNumber(500));
        }
         
        bolaplay bolaplay2 = new bolaplay();
        addObject(bolaplay2,200,330);
        
        judul judul = new judul();
        addObject(judul,(getWidth()/2),153);
      
        start start = new start();
        addObject(start,200,330);
        
        tutorial tutorial = new tutorial();
        addObject(tutorial,200,410);
        
        about about = new about();
        addObject(about,200,490);
        
        
    }
}