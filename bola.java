import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Write a description of class bola here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class bola extends Actor
{
    int x=3;
    int y=3;
    
    /**
     * Act - do whatever the bola wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
        gerak();
        kanan();
        kiri();
        atas();
        papan();       
    }    
    public void gerak()
    {
        setLocation(getX()-x,getY()+y);
    }
    public void kanan()
    {
        if(getX()>=getWorld().getWidth()-getImage().getWidth()/2){
            x=x+1;
            
        }
    }
    public void kiri()
    {
        if(getX()<=getImage().getWidth()/2){
            x=x-1;
        }
    }
    public void atas()
    {        
        if(getY()<=getImage().getHeight()/2){
            y=y+1;
        }
        
        Actor batamerah=getOneIntersectingObject(bata.class);
        if(batamerah !=null){
            getWorld().removeObject(batamerah);
            
            bg bg1 = (bg)getWorld();
            bg1.tambahskor();
            //bg1.sndCoinPlay();
            
            Greenfoot.playSound("coin.mp3");
            
        }
        
        Actor batagoyang=getOneIntersectingObject(batagoyang.class);
        if(batagoyang !=null){
            getWorld().removeObject(batagoyang);
            
            bg bg1 = (bg)getWorld();
            bg1.tambahskor(); 
            bg1.sndCoinPlay();
        }
    }    
    public void papan()
    {
        Actor kayu=getOneIntersectingObject(papan.class);      
        if(kayu !=null){
            y=y-1;
            
        }
        
        if(getY()>=getWorld().getHeight()-getImage().getHeight()/2){
            getWorld().addObject(new gameover(), getWorld().getWidth() /  2, getWorld().getHeight() / 2);
            bg bg1 = (bg)getWorld();
            bg1.stopped();
            bg1.sndKalahPlay();
            
            List remove = getWorld().getObjects( papan.class ); 
            for (Object objects : remove) getWorld().removeObject( ( Actor ) objects );
            getWorld().removeObject(this);
            
        } 
    }
}
