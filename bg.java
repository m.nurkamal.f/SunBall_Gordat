import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

/**
 * Write a description of class bg here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class bg extends World
{

    GreenfootSound backgroundMusic = new GreenfootSound("play.mp3");
    private Counter actCounter;
    private int[][] map = {
                            {1,1,1,1,0},
                            {1,0,0,1,1},
                            {1,0,0,1,1}
                          };
                          
    private int level = 0;
    private bola bola4;
    private papan papan;
    
    private GreenfootSound sndmenang = new GreenfootSound("win.mp3");
    private GreenfootSound sndcoin = new GreenfootSound("coin.mp3");
    private GreenfootSound sndkalah = new GreenfootSound("kalah.mp3");
    
    /**
     * Constructor for objects of class bg.
     * 
     */
    public bg()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(400, 600, 1);
        started();
        prepare();
    }
    public void started(){
        //backgroundMusic.playLoop();
        backgroundMusic.setVolume(50);
        //Greenfoot.setSpeed(55);
    }
    
    public void stopped(){
        backgroundMusic.stop();
        sndmenang.stop();
        sndcoin.stop();
        sndkalah.stop();
    }
   
    
    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
       papan = new papan();
       addObject(papan,190,560);
       
       remap();

       bola4 = new bola();
       addObject(bola4,190,560-(papan.getImage().getHeight()));
        
       actCounter = new Counter("Skor: ");
       addObject(actCounter, 300, 50);
    }
    
    public void tambahskor(){       
       actCounter.setValue(actCounter.getValue() + 1);
       
       //if class bata/ batagoyang null/ empty, size = 0
       
       int count1 = getObjects(bata.class).size();
       int count2 = getObjects(batagoyang.class).size();
             
       if((count1==0) && (count2==0)){             
             level++;
             remap();
             papan.setLocation(190, 560);
             bola4.setLocation(190,560-(papan.getImage().getHeight()));
       }
       
       if(actCounter.getValue() >= 200){
           stopped(); 
                      
           removeObject(bola4);
           List remove = getObjects( papan.class ); 
           for (Object objects : remove) removeObject( ( Actor ) objects );  
           
           sndmenang.play();
           addObject(new win(),getWidth() /  2, getHeight() / 2);
       }
       
    }
    
    public void remap(){
        if(level==0){
           map = new int[][] {
                                {1,0,0,0,1},
                                {1,0,1,0,1},
                                {1,0,2,0,1}
                            };
       }
       
       if(level==1){
           map = new int[][] {
                                {0,1,0,1,0},
                                {0,1,1,0,0},
                                {0,1,0,1,0}
                            };
       }
       
       if(level==2){
           map = new int[][] {
                                {1,0,2,0,0},
                                {0,1,1,0,1},
                                {0,0,1,1,0},
                                {0,0,1,1,1},
                                {0,0,1,1,1},
                                {0,1,1,1,1},
                                {1,1,1,1,1},
                                {1,0,1,1,12}
                            };
       }
       
       if(level > 5){
           map = new int[][]{
                            {1,1,1,1,0},
                            {1,0,0,1,1},
                            {1,0,0,1,1}
            };
        
       }
       
       
       for(int i=0; i<map.length; i++) {
           for(int j=0; j<map[i].length; j++) {
                if(map[i][j]==1){
                    bata x = new bata();
                    int wx= x.getImage().getWidth();
                    int wy= x.getImage().getHeight();
                
                    addObject(x,(j*wx)+20,(i*wy)+20);
                } else if(map[i][j]==2){
                    batagoyang x = new batagoyang();
                    int wx= x.getImage().getWidth();
                    int wy= x.getImage().getHeight();
                
                    addObject(x,(j*wx)+20,(i*wy)+20);
                } else {
                    Kosong x = new Kosong();
                    int wx= x.getImage().getWidth();
                    int wy= x.getImage().getHeight();
                
                    addObject(x,(j*wx)+20,(i*wy)+20);
                
                }
           }
       }
    }
    
    public void sndCoinPlay(){
        sndcoin.play();
    }
    
    public void sndKalahPlay(){
        sndkalah.play();
    }
}
    