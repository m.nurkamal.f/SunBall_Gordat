import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class start here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class start extends Actor
{
    public start() 
    {
        GreenfootImage img=getImage();        
        img.scale(150,75);
        setImage(img);
    }
    public void act() 
    {  
        if(Greenfoot.mouseClicked(this)){
            getWorld().stopped();            
            World start = new bg();
            Greenfoot.setWorld(start);
        }    
    }
}
